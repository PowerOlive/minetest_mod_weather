allow_defined_top = true

read_globals = {
	"minetest",
	"dump", "vector",
	"VoxelManip", "VoxelArea",
	table = { fields = { "copy" } },
	"lightning",
}

ignore = {"211", "212", "213", "631"}